<?php

$post_id 	= $_POST['post_id'];
$user_id 	= $_POST['user_id'];
$redir_url	= $_POST['redir_url'];

require_once('../../../wp-config.php');  
$wp->init();  
$wp->parse_request();  
$wp->query_posts();  
$wp->register_globals(); 

if(read_receipt($user_id, $post_id, $redir_url)){
	// Redirect
	wp_redirect( $redir_url );
	exit;
}else{
	wp_die( "Error updating database!", "Unable to update DB");
}
