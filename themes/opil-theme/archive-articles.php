<?php 

/**
 * This template should only display if Articles is clicked on 
 * as there is a taxonomy template that covers children
 */

get_header(); 

?>

<div class="container">
	<div class="wrapper">
		<?php get_sidebar('help'); ?>	
		<section class="content content-modules" role="main"> 
			<div class="content__heading">
				<h3>
					<?php //post_type_archive_title(); ?>
					Modules
				</h3>
			</div>
			<div class="content__main">

				<?php
				
				//then set the args for wp_list_categories
				$args = array(
				    'parent' 		=> 0,
				    'taxonomy' 		=> 'sections',
					'hide_empty' 	=> 0,
					'hierarchical' 	=> false,
					'orderby'		=> 'menu_order',
					'order'			=> 'DESC',
					'depth'  		=> 1,
					'li_title'		=> ''
				    );
				
				$cats = get_categories( $args );
				?>

				<?php if(!empty($cats)): ?>

					<?php foreach($cats as $cat): ?>

					<div class="module-button button--module">
						<a href="<?php echo get_term_link( $cat ); ?>" class="module-button__title">
							<?php echo $cat->name; ?>
						</a>
						<div class="module-button__stats">

							<span class="module-button__section-count"><?php echo $cat->count; ?> Modules</span>
							<?php 
							if (is_tax()) {
							
								$term_id = $cat->term_id; 
								$args = array(
									'posts_per_page' => -1,
							    	'tax_query' => array(
							 			array(
							     		    'taxonomy' => 'sections',
							     		    'field' => 'id',
							     		    'terms' => $term_id
							     		)
									)
								);

								$query = new WP_Query($args);
								$i = 0;
							?>

							<?php foreach ($query->posts as $post) : ?>
								
								<?php $post_id = $post->ID; ?>

								<?php // Increment $i for every read post ?>
								<?php if(check_progress($user_id,$post_id)) : ?>
									<?php $i++; ?>
								<?php endif; ?>
								
							<?php endforeach; ?>

							<?php // Generate percentage of completion ( number of read posts / total posts ) * 100 ?>
							<?php if ($cat->count >= 1) : ?>
								<?php $progress = floor(($i / $cat->count)*100); ?>
							<?php else : ?>
								<?php $progress = 0; ?>
							<?php endif; ?>

							<?php 
								# code...
							} ?>
						</div>
					</div>

					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</section>
		<?php //get_sidebar('nav'); ?>	
	</div>
</div>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
