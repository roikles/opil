<?php

/**
 * check_form.php
 *
 * This application will mark a quiz made with ACF
 * and update the user_meta with the scores
 *
 * @author Ash Davies <ash.davies@outlook.com>
 */

// Before doing anything make sure we've come from a legit
// exam page instead of being accessed directly.
// 
// If Exam has been submitted we can continue...
// 
// (But first Initiate WP so we can die gracefully)
require_once('../../../wp-config.php');  
$wp->init();  
$wp->parse_request();  
$wp->query_posts();  
$wp->register_globals(); 

if (!empty($_POST)) {

	// Debugging toggle
	$debug = false;
	$redirect = true;

	// Initiate WP
	require_once('../../../wp-config.php');  
	$wp->init();  
	$wp->parse_request();  
	$wp->query_posts();  
	$wp->register_globals(); 

	// Assign current post ID as variable
	$current_post = intval($_POST['post_id']);

	// Assign current user ID as variable
	$current_user = intval($_POST['user_id']);

	// Assign associated section ID as variable;
	$section = intval($_POST['section']);

	// Remove post_id from $_POST
	if (array_key_exists('post_id', $_POST) ) {
		unset($_POST['post_id']);
	}

	// Remove user_id from $_POST
	if (array_key_exists('user_id', $_POST) ) {
		unset($_POST['user_id']);
	}

	// Remove section_id from $_POST
	if (array_key_exists('section', $_POST)) {
		unset($_POST['section']);
	}


	/**
	 * Master array; multidimensional; 
	 * This array stores the question as the key, and the answers as values
	 *
	 * Proposed Structure:
	 * 
	 * - question 		[str] 		( e.g. 'Is the sky blue?' )
	 * - - correct 		[bool(int)] ( e.g. 0 = False, 1 = True )
	 * - - chosen 		[bool(int)]	( See above )
	 * 
	 * @var array
	 */
	$q_and_a = array();


	// Loop through $_POST data
	foreach ($_POST as $question => $answer) { // --- $answer is User's answer NOT default answer

		$question = esc_sql( $question );

		// Query DB for meta_key of current question
		$questions =   "SELECT meta_key
						FROM wp_postmeta
						WHERE post_id = '" . $current_post . "'
						and meta_key = '" . $question . "';";

		$question_meta = $wpdb->get_row($questions,ARRAY_A);

		$question_meta_key = $question_meta['meta_key'];

		// Swap the question key for a default answer key
		//
		// $subject is the original string
		// $search is the thing you want to replace
		// $replace is what you want to replace it with
		$subject 	= $question_meta_key;
		$search 	= 'question';
		$replace 	= 'correct_answer';

		$question_meta_edit = substr_replace($subject, $replace, strrpos($subject, $search), strlen($search));

		// Query DB for value of default answer using key
		$answers = "SELECT meta_value
					FROM wp_postmeta
					WHERE post_id = '" . $current_post . "'
					and meta_key = '" . $question_meta_edit . "';";

		$default_answer_meta = $wpdb->get_row($answers,ARRAY_A);

		// Collect variables again
		$question 		= $question;
		$default_answer = intval($default_answer_meta['meta_value']);
		$their_answer	= intval($answer);

		$q_and_a[$question] = array(
			'correct'	=> $default_answer,
			'chosen'	=> $their_answer
		);
	};

	$total_questions = count($q_and_a);

	// Setup temp array
	$matched_q_and_a = array();

	// Loop through answers and add match result
	foreach ($q_and_a as $q => $a) {

		$correct = $a['correct'];
		$chosen = $a['chosen'];

		// If the chosen answer and user answer match
		// set $match to "true"
		if ($chosen == $correct) {
			$match_check 	= "true";
		}else{
			$match_check	= "false";
		}

		// Add result to temp array
		$matched_q_and_a[$q] = array(
			'correct'	=> $correct,
			'chosen'	=> $chosen,
			'match'		=> $match_check
		);
	}

	// Swap old values for values from temp array
	$q_and_a = array();
	$q_and_a = $matched_q_and_a;
	unset($matched_q_and_a);

	$wrong_ans = array();

	// Count how many answers are correct
	$correct_count = 0;

	foreach ($q_and_a as $key => $value) {
		if ($value['match'] == "true") {
			$correct_count++;
		}else{
			$wrong_ans[$key] = "Incorrect";
		}
	}

	// Calculate pass rate
	$required_percentage = 80;
	$user_percentage = intval(round(($correct_count / $total_questions) * 100));
	$result = false;
	if ($user_percentage >= $required_percentage) {
		$result = true;
	}
	// Run function to add exam ID to database
	update_exam_score($current_post,$current_user);

	// Store user data alongside exam attempt
	$exam_meta = array( $current_post => array(
		'section_id' => $section,
		'section_nicename' => get_term($section,'sections')->name,
		'exam_total_questions' => $total_questions,
		'user_correct_answers' => $correct_count,
		'user_score_percent' => $user_percentage
		)
	);

	$megameta = array( $current_user => $exam_meta );

	add_exam_meta($current_post,$megameta);

	// Fetches question values in DB from a numeric array of wrong answer keys
	$wrong_ans = array_keys($wrong_ans);

} else {

	$title = "Access Denied";
	$message = "You do not have access to this page";
	$args = array(
		'response' => 403,
		'back_link' => true
		);
	wp_die( $message, $title, $args);

};

#####		REDIRECT 		#####

if ($redirect) {
	// Redirect
	wp_redirect( get_permalink( $current_post ) . "?section=" . $section);
	exit;
}


##### 		END REDIRECT 	#####


#####		Debugging 		#####

if ($debug) {
	if ($result) : $result = "Passed"; else : $result = "Failed"; endif;
	echo '<br>';
	echo '<code><h1>Warning: Debug has been Enabled!</h1>';
	echo '<br>';
	echo 'Exam ID: ' . $current_post . "<br>";
	echo 'Examination Section: ' . get_term($section,'sections')->name . "<br>";

	$user = get_userdata( $current_user );

    	echo 'Username: ' . $user->user_login . "<br>";
    	echo 'User roles: ' . implode(', ', $user->roles) . "<br>";
    	echo 'User ID: ' . $user->ID . "<br>";

    echo "Result: ";
		echo $correct_count . " out of " . $total_questions . "<br>";
	echo "You have: " . $result . "<br>";

	echo "<h2>Exam Meta</h2>";
	echo "<pre>";
		print_r( $megameta[$current_user] );
	echo "</pre>";


    echo "<h2>Wrong Answers:</h2>";
    echo "<pre>";
		print_r($wrong_ans);
	echo "</pre>";

	echo "<h2>User's Answers</h2>";
	echo "<pre>";
		print_r($q_and_a);
	echo "</pre>";

	echo '</code>';
}

#####		End Debugging 		#####