<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<section class="content content-index" role="main"> 
			
			<div class="content__heading">
				<h1 class="h2"><?php the_title(); ?></h1>
			</div>
			<div class="content__main">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

					<div class="expert-search">
						<div class="expert-search__box">
							<input type="text" value="" name="s" class="search-input" placeholder="Search by Speciality">
							<input type="submit" value="Go" name="search-button" class="search-button">
						</div>
						<div class="expert-search__box">
							<input type="text" value="" name="s" class="search-input" placeholder="Search by Sub-Speciality">
							<input type="submit" value="Go" name="search-button" class="search-button">
						</div>
						<div class="expert-search__box expert-search__box--last">
							<input type="text" value="" name="s" class="search-input" placeholder="Search by Name of Surgeon">
							<input type="submit" value="Go" name="search-button" class="search-button">
						</div>
					</div>

					<?php the_content(); ?>
					

				<?php endwhile; endif; ?>	
			</div>
		
		</section>	
	</div>
</div>
<?php get_footer(); ?>