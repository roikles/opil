<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<?php get_sidebar('help'); ?>	
		<section class="content content-taxonomy-archive" role="main"> 
			
			<div class="content__heading">
				<h3>
					<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name; ?> Modules
				</h3>
			</div>
			
			<div class="content__main">
				<?php

				//first get the current term
				$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

				//print_r($current_term);

				//then set the args for wp_list_categories
				$args = array(
					'orderby'			=> 'menu_order',
					'order'				=> 'ASC',
				    'child_of' 			=> $current_term->term_id,
				    'parent' 			=> $current_term->term_id,
				    'taxonomy' 			=> $current_term->taxonomy,
					'hide_empty' 		=> 0,
					'hierarchical' 		=> 1,
					'depth'  			=> 1,
					'title_li' 			=> '',
					'echo' 				=> false,
					'show_option_none' 	=> ''
				);

				$cats = get_categories( $args );

				if(is_user_logged_in()){

					// Fetch current_user ID
					$user = wp_get_current_user();
					$user_id = $user->ID;

				};		
				?>

				<?php if(!empty($cats)): ?>

					<?php foreach($cats as $cat): ?>
		
					<div class="module-button button--module">
						<a href="<?php echo get_term_link( $cat ); ?>" class="module-button__title">
							<?php echo $cat->name; ?>
						</a>

						<div class="module-button__stats">

							<span class="module-button__section-count"><?php echo $cat->count; ?> Modules</span>
							<?php 
								$term_id = $cat->term_id; 
								$args = array(
									'posts_per_page' => -1,
							    	'tax_query' => array(
							 			array(
							     		    'taxonomy' => 'sections',
							     		    'field' => 'id',
							     		    'terms' => $term_id
							     		)
									)
								);

								$query = new WP_Query($args);
								$i = 0;
							?>

							<?php foreach ($query->posts as $post) : ?>
								
								<?php $post_id = $post->ID; ?>

								<?php // Increment $i for every read post ?>
								<?php if(check_progress($user_id,$post_id)) : ?>
									<?php $i++; ?>
								<?php endif; ?>
								
							<?php endforeach; ?>

							<?php // Generate percentage of completion ( number of read posts / total posts ) * 100 ?>
							<?php if ($cat->count >= 1) : ?>
								<?php $progress = floor(($i / $cat->count)*100); ?>
							<?php else : ?>
								<?php $progress = 0; ?>
							<?php endif; ?>

							<div class="module-button__completion">
								<span><?php echo $progress . "%"; ?> Complete</span>

								<div class="progress-meter">
									<div class="progress-meter__bar" style="width: <?php echo $progress . "%"; ?>"></div>
								</div>
							</div>
						</div>
					</div>


					<?php endforeach; ?>
				<?php else: ?>

					<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>

						<?php $post_id = $post->ID; ?>

						<?php // Add class to anchor if post has been read ?>
						<a href="<?php the_permalink(); ?>" class="button button--module <?php if(check_progress($user_id,$post_id)) : echo "read-article"; endif; ?>">
							<?php the_title(); ?>
						</a>
						<?php endwhile; ?>

						
				
					<?php else: ?>
						<p>No content found.</p>
						

					<?php endif; ?>	
				
				<?php endif; ?>
			</div>
			
			<div class="exam-link">
				<?php echo generate_exam_button(); ?>
			</div>


			<menu class="pagination">
				<div class="newer"><p><?php previous_posts_link('Newer Entries') ?></p></div>
				<div class="older"><p><?php next_posts_link('Older Entries ','') ?></p></div>
			</menu>
		</section>
		<?php //get_sidebar('nav'); ?>	
	</div>
</div>
<?php get_footer(); ?>
