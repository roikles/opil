<?php
$exam_results = get_current_exam_score($post->ID); 
if ($_POST['section']) {
	$section = $_POST['section']; // --- Current Section ID (e.g. Whiplash/Knee/Foot etc)
}else{
	$section = $_GET['section'];
}

// ...Display results ?>
<div class="content__main">
	<div class="results">

		<div class="score">
			<div class="score__percent">
				<p>
				<?php if ($exam_results->user_score_percent >= 80): ?>
					<strong>You passed, Congratulations!</strong><br>
					Return to the <a href="<?php echo site_url(); ?>" title="Visit the homepage">Homepage</a>
					or <a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout">Logout</a>
				<?php else : ?>
					<strong>Sorry, you haven't passed this attempt</strong><br>
					Would you like to go back and retake the exam to improve your score?
					<?php
						$term = get_terms( 'sections', array('include' => $section));
						$term_link = get_term_link( $term[0], 'sections' );
					?>
			</p>
					
				<?php endif; ?>
				</p>
				<div class="pie-wrapper">
					<div class="c100  big purple p<?php echo $exam_results->user_score_percent; ?>">
						<span class="pie-label">
							<?php echo $exam_results->user_score_percent; ?><span class="pie-percent">&#37;</span>
						</span>
						
						<div class="slice">
							<div class="pie-bar"></div>
							<div class="pie-fill"></div>
						</div>
					</div>
				</div>

				<div class="percent-needed">--- 80&#37; needed to pass ---</div>
			</div>

			
			<div class="score__result">
				<h3>Your Score</h3>
				<p class="score__answers">
					Thank you for attempting this exam.
				</p>
				<p>
					You have correctly answered <?php echo $exam_results->user_correct_answers; ?> out of a total of <?php echo $exam_results->exam_total_questions; ?> questions.
				</p>
				<?php if ($exam_results->user_score_percent < 80 ): ?>
					<form action="<?php echo get_permalink(); ?>" method="POST">
						<input type="hidden" name="retake" value='1'/>
						<input type="hidden" name="section" value='<?php echo $section; ?>'>
						<input type="submit" class="retake-exam" value="Retake Exam"></input>
					</form>
				<?php endif; ?>
					
			</div>
			
		</div>


	</div>
</div>