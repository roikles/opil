<?php get_header(); ?>

<?php

/**
 *  SET NUMBER OF SUB QUESTIONS YOU WANT TO DISPLAY
 *  !important DO NOT DELETE
 */

$number_of_questions = 5;
if ($_POST['section']) {
	$section = $_POST['section']; // --- Current Section ID (e.g. Whiplash/Knee/Foot etc)
}else{
	$section = $_GET['section'];
}
$attempted = is_completed(get_current_user_id(),get_the_ID()); // --- Check if the exam has already been completed
if ($_POST['retake']) {
	$attempted = false;
}
// Set up variables
$current_post 	= get_the_ID();	// --- Current Post ID
$count			= 0; // --- Begin counter for sub-questions
?>

<div class="container">
	<div class="wrapper">
		<?php get_sidebar('help'); ?>	
		<article class="content content-single content-lesson" role="main"> 

			<?php

			// Only display content if user is logged in :)
			if (is_user_logged_in()) :  ?>

			<div class="content__heading">
				<h1 class="h2">
					<?php echo get_term($section,'sections')->name; ?> Examination
					<?php if ($attempted): ?>
						( Results )
					<?php endif; ?>
				</h1>
			</div>
			
				<?php 

				// If exam has been attempted...
				if($attempted) : ?>

					<?php get_template_part( 'lesson-results' ); ?>

				<?php

				// If exam is not completed...
				else : ?>

					<?php

					// ...Display exam questions ?>
					<div class="content__main">

						<h2>Select your answers in the checkboxes below</h2>

						<?php

						// Set up current logged in User ID so we can submit it with their answers on POST
						$current_user = get_current_user_id(); 
						
						// Set up questions as Form so we can submit the user's answers ?>
						<form name="exam-form" class="exam-form" action="<?php echo get_stylesheet_directory_uri(); ?>/check_form.php" method="POST">
						<input type="hidden" name="post_id" value="<?php the_ID(); ?>"/>
						<input type="hidden" name="user_id" value="<?php echo $current_user; ?>"/>
						<input type="hidden" name="section" value="<?php echo $section; ?>"/>

						<table class="single-exam">
							<thead>
								<th >Question</th>
								<th class="truefalse">True / False</th>
							</thead>
							<tbody>
						

						<?php 
							// Query DB for Topic Names
							$topics =  	"SELECT meta_key, meta_value
										FROM wp_postmeta 
										WHERE post_id = '" . $current_post . "'
										AND meta_key
										LIKE 'module_%_topic'
										ORDER BY RAND()";

							if ($section = 6 /* If Section == Whiplash */) :
								// Add limit of Questions to 20
								$topics =  $topics . " LIMIT 20";
							endif;

							
							$topics_meta = $wpdb->get_results($topics,ARRAY_A);

							// For each question title, find the related value in the DB
							foreach ($topics_meta as $key => $value) {

								$meta_key 	= $value['meta_key'];		// --- module_0_topic
								$topic 		= $value['meta_value'];		// --- 'The Sky'

								// Trim '_topic' from end of key to use in SQL statement
								$subject 	= $meta_key;
								$search 	= '_topic';
								$replace 	= '';

								$topic_meta_edit = substr_replace($subject, $replace, strrpos($subject, $search), strlen($search));

								// Fetch all assoc questions from DB for each topic
								$questions =   	"SELECT meta_key, meta_value
												FROM wp_postmeta 
												WHERE post_id = '" . $current_post . "'
												AND meta_key
												LIKE '" . $topic_meta_edit . "_questions_%_question'";

								$questions_meta = $wpdb->get_results($questions,ARRAY_A);

								// Start the game!
								echo '<tr>
										<td class="exam-topic" colspan="2">' . $topic . '</td>
									  </tr>';

								// Shuffle the deck!
								shuffle($questions_meta);

								// Deal the cards!
								for ($count=0; $count < $number_of_questions; $count++) { 

									$question = $questions_meta[$count]['meta_value'];	// --- 'Is it blue?'
									$uniq_key = $questions_meta[$count]['meta_key'];	// --- module_0_questions_2_question

									echo '<tr class="question-row">
											<td width="75%" class="question">
												' . $question . '
											</td>
											<td width="25%" class="answer">

												<input type="radio" class="check-false" id="check-false-' . $uniq_key . '" name="' . $uniq_key . '" value="0"/>
												<label class="answer-radio answer-false" for="check-false-' . $uniq_key . '"></label>
												<input type="radio" class="check-true" id="check-true-' . $uniq_key . '" name="' . $uniq_key . '" value="1"/>
												<label class="answer-radio answer-true" for="check-true-' . $uniq_key . '"></label>
												
											</td>
										  </tr>';
								}

							}

						?>
							</tbody>

						</table>
							<div class="quiz-submit"><input type="submit" value="Submit answers"/></div>
						</form>
					</div>

				<?php endif; ?>
			<?php

			// but if the user is not logged in... :(
			else : ?>

				<div class="content__heading">
					<h1 class="h2">
						You must be logged in to view this Lesson
					</h1>
				</div>

				<div class="content__main">
					<p>If you believe you have recieved this message in error, please email 
						<a href="<?php echo get_option('admin_email'); ?>"><?php echo get_option('admin_email'); ?></a>
					</p>
				</div>

			<?php endif; ?>
		</article>	
	</div>
</div>
<?php get_footer(); ?>
