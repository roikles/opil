<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<section class="content content-index" role="main"> 
			
			<div class="content__heading">
				<h3>
					404 page not found
				</h3>
			</div>
			<div class="content__main">
				<p>There seems to be an issue with the page you are requesting. Sorry.</p>
			</div>
		</section>
		<?php //get_sidebar(); ?>	
	</div>
</div>
<?php get_footer(); ?>