<?php

/**
 * Get lowest child taxonomy that the post beongs to as 
 * we will be redirecting to that below
 * 
 */

$taxonomy = 'sections';
$tax_terms = get_the_terms( $post->ID,$taxonomy );

foreach ($tax_terms as $tax_term){
    $args = array( 'child_of'=> $tax_term->term_id );
    //get all child of current term
    $child = get_terms( $taxonomy, $args );
    if( $tax_term->parent != '0' && count($child) =='0'){
		$parent_section = $tax_term->slug;
    }
}

// check user perms firstif(){}

	if(isset($_POST['submit'])){

		$post_id =			$_POST['id'];
		$current_user =		wp_get_current_user();
		$user_id =			$current_user->ID;
		$meta_key =			'opil_read_articles';	
		$redir_url =		$_POST['redir_url'];	

		read_receipt($post_id, $user_id, $meta_key, $redir_url);
	}

	print_r($_POST['parent_id']);

