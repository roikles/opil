<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<section class="content content-index" role="main"> 
			
			<div class="content__heading">
				<h1 class="h2"><?php the_title(); ?></h1>
			</div>
			<div class="content__main">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
				
					<?php if (!is_user_logged_in()): ?>
					
					<?php the_content(); ?>

					<?php
						$args = array(
							'echo'			=> true,
							'form_id'		=> 'loginform'
							);
					?>
					<?php wp_login_form($args); ?>

					<?php else : ?>
					<?
						global $current_user;
     					get_currentuserinfo();
     				?>

						<p>You are currently signed in as: <?php echo $current_user->user_login; ?> 
							<a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logout">Click here to Logout</a>
						</p>
					<?php endif; ?>

				<?php endwhile; endif; ?>	
			</div>
		
		</section>
		<?php //get_sidebar(); ?>	
	</div>
</div>
<?php get_footer(); ?>