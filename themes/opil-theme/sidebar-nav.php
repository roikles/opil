<?php 
/**
 * Get lowest child taxonomy that the post beongs to as 
 * we will be redirecting to that below
 * 
 */

$taxonomy = 'sections';
$tax_terms = get_the_terms( $post->ID,$taxonomy );
?>

<aside class="content sidebar-nav">			
	<div class="content__heading">
		<h3>Related Articles</h3>
	</div>
	<div class="content__main">

	<?php
		foreach ($tax_terms as $tax_term){
		    $args = array( 'child_of'=> $tax_term->term_id );
		    //get all child of current term
		    $child = get_terms( $taxonomy, $args );
		    if( $tax_term->parent != '0' && count($child) =='0'){
				$parent_section = $tax_term;

				// Assign args for posts with assigned parent term_id
				$args = array( 
					'post_type' => 'articles',
					$taxonomy => $parent_section->slug
					);

				$sections = new WP_Query($args);

				// The Loop
				if ( $sections->have_posts() ) {
					echo '<ul class="section-sidebar">';
					while ( $sections->have_posts() ) {
						$sections->the_post();
						echo '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
					}
					echo '</ul>';
				} else {
					echo 'No related Articles';
				}
				/* Restore original Post Data */
				wp_reset_postdata();
		    }
		}
	 ?>
		 
	</div>
</aside>

	

		