<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<section class="content content-index" role="main"> 
			
			<div class="content__heading">
				<h1 class="h2"><?php the_title(); ?></h1>
			</div>
			<div class="content__main">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
				
					
					<?php the_content(); ?>

				<?php endwhile; endif; ?>	
			</div>
		
		</section>
		<?php //get_sidebar(); ?>	
	</div>
</div>
<?php get_footer(); ?>