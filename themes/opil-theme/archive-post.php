<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<?php get_sidebar('help'); ?>	
		<section class="content content-archive" role="main"> 
			<div class="content__heading">
				<h3><?php the_title(); ?></h3>
			</div>
			<div class="content__main">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('main-article'); ?>>	
						<h3 class="main-article__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</article>
				<?php endwhile; endif; ?>	
			</div>
			<menu class="pagination">
				<div class="newer"><p><?php previous_posts_link('Newer Entries') ?></p></div>
				<div class="older"><p><?php next_posts_link('Older Entries ','') ?></p></div>
			</menu>
		</section>
		<?php get_sidebar('nav'); ?>	
	</div>
</div>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
