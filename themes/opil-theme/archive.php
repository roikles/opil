<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<?php get_sidebar('help'); ?>	
		<section class="content content-archive" role="main"> 
			<div class="content__heading">
				<h3>
					<?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); echo $term->name; ?> Modules
				</h3>
			</div>
			<div class="content__main">


				<?php
				/*
					LIST THE CHILD CATEGORIES WITH A LINK
				*/
				//first get the current term
				$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

				//then set the args for wp_list_categories
				 $args = array(
				    'child_of' 		=> $current_term->term_id,
				    'taxonomy' 		=> $current_term->taxonomy,
					'hide_empty' 	=> 0,
					'orderby'		=> 'menu_order',
					'order'			=> 'asc',
					'hierarchical' 	=> true,
					'depth'  		=> 1,
					'title_li' 		=> ''
				    );
				 wp_list_categories( $args );
				?>


				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('main-article'); ?>>	
						<h3 class="main-article__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					</article>
				<?php endwhile; endif; ?>	
			</div>
			<menu class="pagination">
				<div class="newer"><p><?php previous_posts_link('Newer Entries') ?></p></div>
				<div class="older"><p><?php next_posts_link('Older Entries ','') ?></p></div>
			</menu>
		</section>
		<?php get_sidebar('nav'); ?>	
	</div>
</div>
<?php get_template_part( 'inc/content', 'footer' ); ?>
<?php get_footer(); ?>
