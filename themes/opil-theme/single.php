<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		<?php get_sidebar('help'); ?>	
		<article class="content content-single" role="main"> 
			<div class="content__heading">
				<h1 class="h2">
					<?php the_title(); ?>
				</h1>
			</div>
			<div class="content__main">
				<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
					<div class="webinar">
					<?php // PLACEHOLDER FOR WEBINAR VIDEO - USE THE IFRAME AS EXAMPLE ?>
						<iframe src="//player.vimeo.com/video/38547659?t=0m20s?title=0&amp;byline=0&amp;portrait=0" width="528" height="275" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					</div>
					<?php the_content(); ?>
				<div class="edu-article-section accordion" id="<?php echo $section_hash; ?>">
					<?php if(get_field('sections')): ?>
						
						<?php while(has_sub_field('sections')): ?>
					 		<?php 
				 				$section = get_sub_field('section_title'); 
				 				$section_hash = strtolower(str_replace(" ","-",get_sub_field('section_title')));
				 			?>
				 			
								<div class="accordion__header">
									<h3 class="accordion__title"><?php the_sub_field('section_title'); ?></h3>
									<div class="accordion__button-con">
										<button class="button button--small"></button>
									</div>
								</div> 
								<div class="accordion__content">
									<?php the_sub_field('section_contents'); ?>									
								</div>
								
						<?php endwhile; ?>
						
					<?php endif; ?>
				</div>
					<?php if(get_field('references')): ?>
					<div class="accordion">
						<div class="accordion__header">
							<h3 class="accordion__title">References</h3>
							<div class="accordion__button-con">
								<button class="button button--small"></button>
							</div>
						</div>	
					
						<div class="accordion__references">
							<?php while(has_sub_field('references')): ?>
								<div class="accordion__header">
									<p class="accordion__title"><em><?php the_sub_field('reference_title'); ?></em></p>
									<div class="accordion__button-con">
										<button class="button button--small"></button>
									</div>
								</div>
							<div class="accordion__content">
								<?php the_sub_field('reference_abstract'); ?>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
					<?php endif; ?>

					<?php
					// Check is user is logged in
					if(is_user_logged_in()){

						// Fetch current post_id
						$post_id = $post->ID;

						// Fetch current_user ID
						$user = wp_get_current_user();
						$user_id = $user->ID;

						// Find lowest post taxonomy and generate URI
						$lowest_tax = lowest_post_taxonomy($post_id);
						$redir_url = get_term_link($lowest_tax);
					?>
					
					<form action="<?php echo get_stylesheet_directory_uri(); ?>/update_read_receipt.php" method="POST">
						<input type="hidden" name="post_id" value="<?php echo $post_id; ?>">
						<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
						<input type="hidden" name="redir_url" value="<?php echo $redir_url; ?>">
						<input type="submit" class="complete-article" value="Complete this Article" name="submit">
					</form>

					<?php }else{ // If user is not logged in ?>

					<h4>Please log in to mark this module as read.</h4>
					<p>
						<a href="<?php echo wp_login_url( get_permalink(), $force_reauth = true ); ?>" title="Login">
							Click here to login.
						</a>
					</p>

					<?php }; ?>
				<?php endwhile; endif; ?>	
			</div>
		</article>
		<?php get_sidebar('nav'); ?>	
	</div>
</div>
<?php get_footer(); ?>
