<div class="site-footer">
	<footer class="wrapper">
		<nav class="footer-nav">
			<ul class="footer-nav__menu">
			<?php 
				//this will fail unless location is defined
				$args = array(
					'menu'       => 'footer-menu',
					'container'  => false,
					'items_wrap' => '%3$s'
				);
			 	wp_nav_menu( $args ); 
			 ?>
				
			</ul>
		</nav>
	</footer>
</div>
<div class="gridtacular"></div>
<pre><?php //print_r( debug_backtrace() ); ?></pre>


<?php wp_footer(); //important! ?>
</body>
</html>