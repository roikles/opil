<aside class="sidebar-help">
	<figure class="help-button help-button--expert">
		<a href="#">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sidebar/expert.jpg" alt="medico legal professional in a library">
			<figcaption><p>Ask an expert</p></figcaption>
		</a>
	</figure>
	<figure class="help-button help-button--glossary">
		<a href="#">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/sidebar/glossary.jpg" alt="magnifying glass over a dictionary">
			<figcaption><p>Glossary of terms</p></figcaption>
		</a>
	</figure>
</aside>

	

		