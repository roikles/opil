# Require any additional compass plugins here.

http_path = "/"
css_dir = "../"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "javascripts"

output_style = :compact

preferred_syntax = :scss

line_comments = false # by Fire.app 
output_style = :nested # by Fire.app 
Sass::Script::Number.precision = 10