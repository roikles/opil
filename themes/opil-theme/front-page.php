<?php get_header(); ?>
<div class="container">
	<div class="wrapper">
		
		<?php // SLIDER ?>
		<div class="slider home-slider">
			<div class="inner">
				<ul class="bjqs">
			         <li>
			        	<img class="slider__image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/slider/gavel-and-scales.jpg" alt="gavel and scales - symbols of the legal profession">
						<figcaption class="slider__caption">
							<p class="slider__caption__text">The online personal injury learning tool that keeps you up to date with all that is new in medico legal practice.</p>
							<a href="#" class="button slider__caption__button">Join Today</a>
						</figcaption>
			        </li>
			        <li>
			        	<img class="slider__image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/slider/gavel-2.jpg" alt="gavel and scales - symbols of the legal profession">
						<figcaption class="slider__caption">
							<p class="slider__caption__text">The online personal injury learning tool that keeps you up to date with all that is new in medico legal practice.</p>
							<a href="#" class="button slider__caption__button">Join Today</a>
						</figcaption>
			        </li>
			    </ul>
			</div>
		</div>

		<?php // NEWS ?>
		
		<div class="content news-home">
			<div class="content__heading">
				<h1 class="h2">News</h1>
			</div>
			<div class="content__main">

				<?php
					/**
					 * List posts based on args
					 * This pulls in all uncategorized and news posts
					 * Limited to 3 articles
					 */
					
					$args = array(
						'post_type' 	 => 'post',
                        'posts_per_page' => 3
					);

                    $post_query = new WP_Query( $args );

                    if ( $post_query->have_posts() ): while ( $post_query->have_posts() ) : $post_query->the_post(); ?>

                   	<article class="news-snippet">
                        <p class="news-snippet__text"><?php the_title(); ?></p>
                        <a href="<?php the_permalink(); ?>" class="read-more news-snippet__read-more">Read More</a>
                	</article>
                                
        		<?php endwhile; endif;  ?>

			</div>
		</div>
	</div>

	<div class="wrapper">

		<section class="content content-home" role="main"> 
			<?php if ( have_posts() ): while ( have_posts() ) : the_post(); ?>
				
				<div class="content__heading">
					<h1 class="h2"><?php the_title(); ?></h1>
				</div>
				<div class="content__main">
					<p><?php the_content(); ?></p>
				</div>
				
			<?php endwhile; endif; ?>	
		</section>
	
	</div>
</div>
<?php get_footer(); ?>