<?php

/*==================================================================== */
/* CONTENT WIDTH
/*==================================================================== */

	if ( ! isset( $content_width )){
		$content_width = 620; //GLOBAL CONTENT WIDTH (px)
	}


/*==================================================================== */
/* CBG THEME SETUP
/*==================================================================== */


	add_action( 'after_setup_theme', 'flexbones_setup' );
	
	function flexbones_setup() {
	
		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style('editor-style.css?' . time());
	
		// Post Format support. You can also use the legacy "gallery" or "asides" (note the plural) categories.
		add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
	
		// This theme uses post thumbnails
		add_theme_support( 'post-thumbnails' );
	
		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );
	
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => 'Primary Navigation',
		) );
		
		//register sidebars
		register_sidebar();
	}

/*==================================================================== */
/* SET THE EXCERPT LENGTH
/*==================================================================== */

	function cbg_excerpt_length( $length ) {
		return 55;
	}
	
	add_filter( 'excerpt_length', 'cbg_excerpt_length' );
	

/*==================================================================== */
/* SET THE READ MORE TEXT
/*==================================================================== */

	/* Replaces the excerpt "more" text by a link */

	function new_excerpt_more($more) {
	    global $post;
	    return '... <a href="'. get_permalink($post->ID) . '" class="read-more">' . 'View more.' . '</a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');


/*==================================================================== */
/* Remove inline styles printed when the gallery shortcode is used
/*==================================================================== */

	add_filter( 'use_default_gallery_style', '__return_false' );
	
/*==================================================================== */
/* ADD GALLERY THUMBNAIL CUSTOM SIZE 
/*==================================================================== */

//add_image_size( 'gallery-thumb', 300, 195, true );

/*==================================================================== */
/* STOP TINY MCE Editing BRS
/*==================================================================== */

function cbnet_tinymce_config( $init ) {

    // Don't remove line breaks
    $init['remove_linebreaks'] = false; 

    // Pass $init back to WordPress
    return $init;
}
add_filter('tiny_mce_before_init', 'cbnet_tinymce_config');

/*==================================================================== */
/* REMOVE Paragraphs round IMAGES 
/*==================================================================== */

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');

/*==================================================================== */
/* REMOVE IMAGE DIMENSIONS (for responsive imgs)
/*==================================================================== */

add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10 ); 
add_filter( 'image_send_to_editor', 'remove_thumbnail_dimensions', 10 ); 
add_filter( 'the_content', 'remove_thumbnail_dimensions', 10 );

function remove_thumbnail_dimensions( $html ) { 
	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html ); return $html; 
}

/*==================================================================== */
/* REMOVE ADMIN BAR 
/*==================================================================== */

add_filter('show_admin_bar', '__return_false');

/*==================================================================== */
/* SUB MENU
/*==================================================================== */	
	
function sub_menu(){
	
	global $post;
	
	if($post){
		
		if($post->post_parent){
			$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
		} else {
			$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
		} 
		
		if ($children) {
			echo '<ul>' . $children . '</ul>';
		}
	}
}

/*==================================================================== */
/* Add parent class to Wp list pages! great for recursive menus!
/*==================================================================== */	

function add_parent_class( $css_class, $page, $depth, $args ){
    if ( ! empty( $args['has_children'] ) )
        $css_class[] = 'parent slide_down';
    return $css_class;
}
add_filter( 'page_css_class', 'add_parent_class', 10, 4 );

/*==================================================================== */
/* Add parent class to wp_nav_menu
/*==================================================================== */       

add_filter('wp_nav_menu_objects', function ($items) {

    $hasSub = function ($menu_item_id, $items) {
        foreach ($items as $item) {
            if ($item->menu_item_parent && $item->menu_item_parent == $menu_item_id) {
                return true;
            }
        }
        return false;
    };

    foreach ($items as $item) {
        if ($hasSub($item->ID, $items)) {
            $item->classes[] = 'parent';
        }
    }
    return $items;    

});

/*==================================================================== */
/* Wrap Li's in spans from the content to allow for greater styling control
/*==================================================================== */	

function span_li($content){
	
	$content = str_replace( '<li>','<li><span>',$content );
	$content = str_replace( '</li>','</span></li>',$content );
	return $content;
	
}

add_filter( 'the_content', 'span_li' );

/*==================================================================== */
/* Enque JS Files 
/*==================================================================== */

function barebones_load_js() {
 	// NAME / LOCATION / DEPENDENCIES (accepts array) / VERSION / IN FOOTER (true | false)
  	wp_register_script( 'sitewide-scripts', get_template_directory_uri( ) . '/js/scripts.js', array( 'jquery' ), '1', true );
  	wp_enqueue_script( 'sitewide-scripts' );
  	//wp_register_script( 'gridtacular', get_template_directory_uri( ) . '/js/gridtacular.js', array( 'jquery' ), '1', true );
  	//wp_enqueue_script( 'gridtacular' );
	wp_enqueue_script( 'jquery-ui-core' );  
	wp_enqueue_script( 'jquery-ui-accordion' );
  	
  	//set the stylesheet directory uri to var 'stylesheet_root' and pass the var to documents that require it

  	$stylesheet_root = array( 'dir' => get_stylesheet_directory_uri() );
    wp_localize_script( 'sitewide-scripts', 'stylesheet_root', $stylesheet_root );
    wp_localize_script( 'gridtacular', 'stylesheet_root', $stylesheet_root );

    //useage in JS : stylesheet_root.dir

	/* if ( is_front_page() ) {
		wp_enqueue_script('home-page-main-flex-slider');
	}*/
 
}
 
add_action('wp_enqueue_scripts', 'barebones_load_js'); // For use on the Front end (ie. Theme)

/*==================================================================== */
/* Enque Stylesheet
/*==================================================================== */

function stylesheet_loader() {
	wp_register_style( 
		'page-style', 
    	get_template_directory_uri() . '/style.css', 
    	array(), 
    	'4.0', 
    	'all' 
    );

  // enqueing:
  wp_enqueue_style( 'page-style' );
}

add_action( 'wp_enqueue_scripts', 'stylesheet_loader' );


/*==================================================================== */
/* Override Default WordPress Search behaviour to surpress 404 redirect
/*==================================================================== */

function SearchFilter($query) {
    // If 's' request variable is set but empty 
    if (isset($_GET['s']) && empty($_GET['s']) && $query->is_main_query()){
        $query->is_search = true;
        $query->is_home = false;
    }
    return $query;
}

add_filter('pre_get_posts','SearchFilter');

/*==================================================================== */
/* Wrap images in div
/*==================================================================== */

if( is_admin() ) {
 
    add_filter( 'image_send_to_editor', 'wp_image_wrap_init', 10, 8 );    
    function wp_image_wrap_init( $html, $id, $caption, $title, $align, $url, $size, $alt ) {
        return '<div class="content-image">'. $html .'</div>';
    }
 
}
/*==================================================================== */
/* Section progress meter
/*==================================================================== */

/**
 * Generates percentage of completion for a specified section
 * 
 * @param  [int] $user_id [current user ID]
 * @param  [int] $post_id [current post ID]
 * @author  Ash Davies [ash.davies@outlook.com]
 */
function check_progress($user_id){

	$post_id = get_the_ID();

	if(is_tax('sections')) {
		// Setup meta_key
		$meta_key = 'opil_read_articles';
	
		// Fetch existing serialized meta
		$s_old_meta = get_user_meta( $user_id, $meta_key);

		if ($s_old_meta) {
			// Unserialize meta and return to array
			$old_meta = maybe_unserialize( $s_old_meta );
		
			// Assign read posts array as variable
			$read_posts = $old_meta[0];
		
			// If current post is not in the meta array, end the function otherwise return true
			if (!in_array($post_id, $read_posts)) {
				return false;
			}
		
			return true;
		}else{
			return false;
		}
	
		
	}else{
		return false;
	}
}

/*==================================================================== */
/* Article Read Receipts
/*==================================================================== */

// NEEDS A CAPABILITIES CHECK WRAPPER


/**
 * On completion read_receipt updates the database
 * to add the current article to the list of completed 
 * articles.
 *
 * @param  [int] $user_id  [current user ID]
 * @param  [int] $post_id  [current post ID]
 * @param  [str] $redir_url [URL to redirect to, once DB is updated]
 *
 * @author  Ash Davies [ash.davies@outlook.com]
 */
function read_receipt($user_id, $post_id, $redir_url){

	// Setup meta_key
	$meta_key = 'opil_read_articles';

	// Get user meta 'read-articles' as array	
	$get_meta = get_user_meta($user_id, $meta_key);

	// Setup array with post_id
	$post_array = array( $post_id );

	if(empty($get_meta)){ // If no user_meta exists...

		// Push auto-serialized meta to DB
		add_user_meta( $user_id, $meta_key, $post_array);
		
	} else { // If user_meta exists..

		// Fetch existing serialized meta
		$s_old_meta = get_user_meta( $user_id, $meta_key );

		// Unserialize meta and return to array
		$old_meta = maybe_unserialize( $s_old_meta );

		// Check to see if current post_id is already in the array
		// If no duplicate post_ids are found..
		if(!in_array($post_id, $old_meta[0])){

			// Merge old array with new array
			$new_meta = array_merge($old_meta[0], $post_array);

			// Push auto-serialized meta to DB
			update_user_meta( $user_id, $meta_key, $new_meta );
		};

	};
	return true;
}



/**
 * Creates a dynamic take-the-test link if the current section
 * has an associated lesson link.
 * 
 * Associated Link is defined in WP-Admin > <section> > Edit
 * 
 */
function generate_exam_button(){

	$queried_object = get_queried_object(); 
	$taxonomy = $queried_object->taxonomy;
	$term_id = $queried_object->term_id;  

	$term = get_term_by( id, $term_id, $taxonomy);

	$acf_current_term = $taxonomy . '_' . $term_id;

	$associated_lesson = get_field('associated_lesson', $acf_current_term);

	if ($associated_lesson) {

		// get associated_lesson URL for this taxonomy term
		if (check_if_all_articles_are_read()){
			$class = "active-exam";
			$url = '<form action="' . $associated_lesson . '" method="POST">
						<input type="hidden" name="section" value="' . $term_id . '"/>
						<input type="submit" class="' . $class . '" value="Take the ' . $term->name . ' Exam"/>
					</form>';
		}else{
			$class = "inactive-exam";
			$associated_lesson = "#";
			$url = '<p class="' . $class . '">Please complete all of the Articles to continue</p>';
		}

		return $url;
	}else{
		return false;
	}
}


function check_if_all_articles_are_read(){
	$read = false;
	$posts = get_assoc_posts_for_current_term();

	if (is_user_logged_in()) {
		// Setup User ID
		$user_id = get_current_user_id();
		// Setup meta_key
		$meta_key = 'opil_read_articles';

		// Get user meta 'read-articles' as array	
		$get_meta = get_user_meta($user_id, $meta_key);
		
		if (!empty($get_meta)) {
			// Check if all post IDs match read posts
			$compare = array_diff($posts, $get_meta[0]);
			if( empty($compare) ) {
				$read = true;
			}
		}
	}
	return $read;
	
}

/**
 * Adds each exam attempt to the wp_exammeta table and stores meta which includes
 * [user_id]
 * [exam_id]
 * -- [section_id]
 * -- [section_nicename]
 * -- [exam_total_questions]
 * -- [user_correct_answers]
 * -- [user_score_percent]
 * @param [type] $current_user [description]
 * @param [type] $megameta     [description]
 */
function add_exam_meta($current_post,$megameta){
	global $wpdb;

	$keys = array_keys($megameta);
	$current_user = $keys[0];

	$exam_meta = $megameta[$current_user];
	$s_exam_meta = maybe_serialize( $exam_meta );

	// Check to see if values exist
	$sql_check = "SELECT *
				FROM wp_exammeta
				WHERE user_id = '" . $current_user . "'
				AND exam_id = '" . $current_post . "'";

	// Overwrite if they do
	if ($check_meta = $wpdb->get_results($sql_check)) {
		$wpdb->update(
		'wp_exammeta',	
			array(
				'user_correct_answers'	=>	$exam_meta[$current_post]['user_correct_answers'],
				'user_score_percent'	=>	$exam_meta[$current_post]['user_score_percent'],
				'exam_total_questions'	=>	$exam_meta[$current_post]['exam_total_questions']
			),
			array(
				'user_id'	=>	$current_user,
				'exam_id'	=>	$current_post
			)
		);
	// Else insert
	}else{
		$meta_keys = array_keys($exam_meta);
		$wpdb->insert(
		'wp_exammeta',	
			array(
				'user_id'				=>	$current_user,
				'exam_id'				=>	$meta_keys[0],
				'section_id'			=>	$exam_meta[$current_post]['section_id'],
				'section_nicename'		=>	$exam_meta[$current_post]['section_nicename'],
				'exam_total_questions'	=>	$exam_meta[$current_post]['exam_total_questions'],
				'user_correct_answers'	=>	$exam_meta[$current_post]['user_correct_answers'],
				'user_score_percent'	=>	$exam_meta[$current_post]['user_score_percent']
			)
		);
	}
	return true;
}

/**
 * This function will add the post_id of the examination into the user_meta
 * table if the user has attained a pass.
 *
 * @param [int]		$current_post 	The current examination ID
 * @param [int] 	$current_user 	The current user ID
 *
 * @return [bool] 
 */
function update_exam_score($current_post,$current_user){

	// Setup meta_key
	$meta_key = 'opil_completed_exams';

	// Get user meta 'opil_completed_exams' as array	
	$get_meta = get_user_meta($current_user, $meta_key);

	// Setup array with post_id
	$exam_array = array( $current_post );

	if(empty($get_meta)){ // If no user_meta exists...

		// Push auto-serialized meta to DB
		add_user_meta( $current_user, $meta_key, $exam_array);

		return true;
		
	} else { // If user_meta exists..

		// Fetch existing serialized meta
		$s_old_meta = get_user_meta( $current_user, $meta_key );

		// Unserialize meta and return to array
		$old_meta = maybe_unserialize( $s_old_meta );

		// Check to see if current post_id is already in the array
		// If no duplicate post_ids are found..
		if(!in_array($current_post, $old_meta[0])){

			// Merge old array with new array
			$new_meta = array_merge($old_meta[0], $exam_array);

			// Push auto-serialized meta to DB
			update_user_meta( $current_user, $meta_key, $new_meta );
			return true;
		}else{
			return false;
		}

	};
};

function get_current_exam_score($post){

	global $wpdb;

	$sql = "SELECT exam_total_questions, user_correct_answers, user_score_percent
			FROM wp_exammeta
			WHERE user_id = '" . get_current_user_id() . "'
			AND exam_id = '" . $post . "'";

	$exam_meta = $wpdb->get_row($sql,OBJECT);

	$result = "Failed";

	if ($exam_meta->user_score_percent >= 80) {
		$result = "Passed";
	}

	$exam_meta->result = $result;

	return $exam_meta;
}


/**
 * This function will check to see if the current examination
 * has already been completed
 * @param  [int]  $user Current User ID
 * @param  [int]  $post Current Exam ID
 * 
 * @return [bool]
 */
function is_completed($user,$post){

	// Setup meta_key
	$meta_key = 'opil_completed_exams';

	// Get user meta 'opil_completed_exams' as array	
	$get_meta = get_user_meta($user, $meta_key);

	if (!empty($get_meta)) {
		// Unserialize meta and return to array
		$completed_exams = maybe_unserialize( $get_meta );

		// Test if current exam ID has been completed already
		if (in_array($post, $completed_exams[0])) {
			return true;
		}else{
			return false;
		}
	}
}

function get_assoc_posts_for_current_term(){
	global $posts;
	$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_id = $current_term->term_id;

	foreach ($posts as $post){
		$posts_in_current_term[] = $post->ID;
	}

	return $posts_in_current_term;
}


function taxonomy_heirarchy( $terms = 'sections' ){

	// If terms is not array or its empty don't proceed
    if ( ! is_array( $terms ) || empty( $terms ) ) {
        return false;
    }

    foreach ( $terms as $term ) {
        // if the term have a parent, set the child term as attribute in parent term
        if ( $term->parent != 0 )  {
            $terms[$term->parent]->child = $term;   
        } else {
            // record the parent term
            $parent = $term;
        }
    }

    $current_taxonomies = array(
    	'parent_name' 		=> $parent->name,
    	'parent_id' 		=> $parent->term_id,
    	'child_name' 		=> $parent->child->name,
    	'child_id' 			=> $parent->child->term_id,
    	'grandchild_name' 	=> $parent->child->child->name,   
    	'grandchild_id' 	=> $parent->child->child->term_id    
    );

    return $current_taxonomies;
}

/**
 * Custom Post Type for Articles
 */

function post_type_articles() {
	register_post_type(
		'articles', array(	
			'label' 			 => 'Articles',
			'description' 		 => 'Add articles to the site.',
			'public' 			 => true,
			'show_ui' 			 => true,
			'show_in_menu' 		 => true,
			'capability_type' 	 => 'post',
			'hierarchical' 		 => true,
			'rewrite' 			 => array(
				'slug' 				=> '',
				'with_front' 		=> '0'
			),
			'query_var' 		 => true,
			'has_archive' 		 => true,
			'supports' 			 => array(
				'title',
				'editor',
				'revisions',
				'thumbnail',
				'author',
				'page-attributes'
			),
			'taxonomies' 		=> array(
				'post_tag',
				'sections'
			),
			'labels' 			=> array(
				'name' 				 => 'Articles',
				'singular_name' 	 => 'Article',
				'menu_name' 		 => 'Articles',
				'add_new' 			 => 'Add Article',
				'add_new_item' 		 => 'Add New Article',
				'edit' 				 => 'Edit',
				'edit_item' 		 => 'Edit Article',
				'new_item' 			 => 'New Article',
				'view' 				 => 'View Article',
				'view_item' 		 => 'View Article',
				'search_items' 		 => 'Search Articles',
				'not_found' 		 => 'No Articles Found',
				'not_found_in_trash' => 'No Articles Found in Trash',
				'parent' 			 => 'Parent Article'
			),
		) 
	);
}

add_action( 'init', 'post_type_articles' );

/**
 * Create Taxonomy: Sections
 */

function taxonomy_sections(){
	register_taxonomy(
		'sections',array (
	  		0 => 'articles',
		),array( 
			'hierarchical'   => true,
			'label' 		 => 'Sections',
			'show_ui' 		 => true,
			'query_var' 	 => true,
			'rewrite' 		 => array('hierarchical' => true, 'with_front' => false),
			'singular_label' => 'Section'
		) 
	);
}

add_action('init', 'taxonomy_sections' );

/**
 * Create Array of taxonomy siblings
 * @param $term_id = the term id you want the siblings of
 * @param $taxonomy = the taxonomy name you wish to  use
 * @param  $args [OPTIONAL] accepts set of wpdb arguments to query
 * @return Array of taxonomies + links
 */

function get_taxonomy_siblings($parent_term_id, $taxonomy, $args = ''){
	
	if( !isset( $args ) ){
		$args = array( 
			'parent'  		=> $parent_term_id,
			'hide_empty'	=> false,
			'orderby'		=> 'menu_order',
			'order'			=> 'ASC',
			'depth'  		=> 2,
			'hierarchical'  => false
		);
	}

	$siblings = get_categories( $taxonomy, $args );

	$sibling_array = array();

	foreach( $siblings as $sibling ){

		$sibling_array[$sibling->name] = array( );
		
		$sibling_array[$sibling->name]['name'] = $sibling->name;
		
		if( !is_wp_error( get_term_link( $sibling->name, $taxonomy ) ) ){
			$sibling_array[$sibling->name]['permalink'] = get_term_link( $sibling->name, $taxonomy );	
		}
	}

	return $sibling_array;
}

/**
 * lowest_post_taxonomy
 * retrieves the lowest hierarchical taxonomy term
 * for a given post_id
 * 
 * @param  [int] $post_id current post_id
 * @return [array] taxonomy term metadata
 */

function lowest_post_taxonomy($post_id){
	$taxonomy = 'sections';
	$tax_terms = get_the_terms( $post_id,$taxonomy );

	// Return terms as array
	return end($tax_terms);
}


/**
 * Create a custom Post Type
 * Much better than using a plugin
 * @author Rory Ashford <rory@roikles.com>
 */

register_post_type(
	'lessons', array(	
		'label' 			 => 'Lessons',
		'description' 		 => 'Add Lessons to the site.',
		'public' 			 => true,
		'show_ui' 			 => true,
		'show_in_menu' 		 => true,
		'capability_type' 	 => 'post',
		'hierarchical' 		 => true,
		'rewrite' 			 => array(
			'slug' 				=> 'lesson',
			'with_front' 		=> '0'
		),
		'query_var' 		 => true,
		'has_archive' 		 => true,
		'supports' 			 => array(
			'title',
			'revisions'
		),
		'taxonomies' 		=> array(
			'post_tag',
			'sections',
		),
		'labels' 			=> array(
			'name' 				 => 'Lessons',
			'singular_name' 	 => 'Article',
			'menu_name' 		 => 'Lessons',
			'add_new' 			 => 'Add Lesson',
			'add_new_item' 		 => 'Add New Lesson',
			'edit' 				 => 'Edit',
			'edit_item' 		 => 'Edit Lesson',
			'new_item' 			 => 'New Lesson',
			'view' 				 => 'View Lesson',
			'view_item' 		 => 'View Lesson',
			'search_items' 		 => 'Search Lessons',
			'not_found' 		 => 'No Lessons Found',
			'not_found_in_trash' => 'No Lessons Found in Trash',
			'parent' 			 => 'Parent Lesson'
		),
	) 
);


/**
 * This function will return an ID for Lesson that has been
 * associated with the current taxonomy term
 * 
 * @param  [str] $field    	The ACF Field slug ('associated_lesson' usually )
 * @param  [str] $taxonomy 	The Taxonomy slug ('sections' usually)
 * @return [str]          	Returns the ID of the associated lesson as a string 
 */
function fetch_associated_exam_id($field,$taxonomy){
	$queried_object = get_queried_object(); 
	$taxonomy = $queried_object->taxonomy;
	$term_id = $queried_object->term_id;  

	$term = get_term_by( id, $term_id, $taxonomy);

	$acf_current_term = $taxonomy . '_' . $term_id;

	$associated_lesson = get_field($field, $acf_current_term);

	if ($associated_lesson) {
		return convert_url_to_postid($associated_lesson);
	}else{
		return false;
	}
}




/**
 * Post URLs to IDs function, supports custom post types - borrowed and modified from url_to_postid() in wp-includes/rewrite.php
 *
 * @author "betterwp.net/wordpress-tips/url_to_postid-for-custom-post-types/"
 */
function convert_url_to_postid($url)
{
	global $wp_rewrite;

	$url = apply_filters('url_to_postid', $url);

	// First, check to see if there is a 'p=N' or 'page_id=N' to match against
	if ( preg_match('#[?&](p|page_id|attachment_id)=(\d+)#', $url, $values) )	{
		$id = absint($values[2]);
		if ( $id )
			return $id;
	}

	// Check to see if we are using rewrite rules
	$rewrite = $wp_rewrite->wp_rewrite_rules();

	// Not using rewrite rules, and 'p=N' and 'page_id=N' methods failed, so we're out of options
	if ( empty($rewrite) )
		return 0;

	// Get rid of the #anchor
	$url_split = explode('#', $url);
	$url = $url_split[0];

	// Get rid of URL ?query=string
	$url_split = explode('?', $url);
	$url = $url_split[0];

	// Add 'www.' if it is absent and should be there
	if ( false !== strpos(home_url(), '://www.') && false === strpos($url, '://www.') )
		$url = str_replace('://', '://www.', $url);

	// Strip 'www.' if it is present and shouldn't be
	if ( false === strpos(home_url(), '://www.') )
		$url = str_replace('://www.', '://', $url);

	// Strip 'index.php/' if we're not using path info permalinks
	if ( !$wp_rewrite->using_index_permalinks() )
		$url = str_replace('index.php/', '', $url);

	if ( false !== strpos($url, home_url()) ) {
		// Chop off http://domain.com
		$url = str_replace(home_url(), '', $url);
	} else {
		// Chop off /path/to/blog
		$home_path = parse_url(home_url());
		$home_path = isset( $home_path['path'] ) ? $home_path['path'] : '' ;
		$url = str_replace($home_path, '', $url);
	}

	// Trim leading and lagging slashes
	$url = trim($url, '/');

	$request = $url;
	// Look for matches.
	$request_match = $request;
	foreach ( (array)$rewrite as $match => $query) {
		// If the requesting file is the anchor of the match, prepend it
		// to the path info.
		if ( !empty($url) && ($url != $request) && (strpos($match, $url) === 0) )
			$request_match = $url . '/' . $request;

		if ( preg_match("!^$match!", $request_match, $matches) ) {
			// Got a match.
			// Trim the query of everything up to the '?'.
			$query = preg_replace("!^.+\?!", '', $query);

			// Substitute the substring matches into the query.
			$query = addslashes(WP_MatchesMapRegex::apply($query, $matches));

			// Filter out non-public query vars
			global $wp;
			parse_str($query, $query_vars);
			$query = array();
			foreach ( (array) $query_vars as $key => $value ) {
				if ( in_array($key, $wp->public_query_vars) )
					$query[$key] = $value;
			}

		// Taken from class-wp.php
		foreach ( $GLOBALS['wp_post_types'] as $post_type => $t )
			if ( $t->query_var )
				$post_type_query_vars[$t->query_var] = $post_type;

		foreach ( $wp->public_query_vars as $wpvar ) {
			if ( isset( $wp->extra_query_vars[$wpvar] ) )
				$query[$wpvar] = $wp->extra_query_vars[$wpvar];
			elseif ( isset( $_POST[$wpvar] ) )
				$query[$wpvar] = $_POST[$wpvar];
			elseif ( isset( $_GET[$wpvar] ) )
				$query[$wpvar] = $_GET[$wpvar];
			elseif ( isset( $query_vars[$wpvar] ) )
				$query[$wpvar] = $query_vars[$wpvar];

			if ( !empty( $query[$wpvar] ) ) {
				if ( ! is_array( $query[$wpvar] ) ) {
					$query[$wpvar] = (string) $query[$wpvar];
				} else {
					foreach ( $query[$wpvar] as $vkey => $v ) {
						if ( !is_object( $v ) ) {
							$query[$wpvar][$vkey] = (string) $v;
						}
					}
				}

				if ( isset($post_type_query_vars[$wpvar] ) ) {
					$query['post_type'] = $post_type_query_vars[$wpvar];
					$query['name'] = $query[$wpvar];
				}
			}
		}

			// Do the query
			$query = new WP_Query($query);
			if ( !empty($query->posts) && $query->is_singular )
				return $query->post->ID;
			else
				return 0;
		}
	}
	return 0;
}




